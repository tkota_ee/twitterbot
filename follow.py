#  -*- coding: utf-8 -*-
# Tweepyライブラリをインポート
import tweepy
import time
import sys
import pprint
import setting
import os
import json
import requests
import feedparser
import ssl
import random
from requests_oauthlib import OAuth1Session

pp = pprint.PrettyPrinter(indent=4)

# Setting for each keys
CK= setting.CK
CS= setting.CS
AT= setting.AT
AS= setting.AS
SN= setting.SN

# auth setting
auth = tweepy.OAuthHandler(CK, CS)
auth.set_access_token(AT, AS)

# create API instance
api = tweepy.API(auth)
# argv = sys.argv
# interval for each action
interval = 10
file_list = []

# For 
BLUE = '\033[94m'
BBLUE = '\033[44m'
GREEN = '\033[92m'
RED = '\033[93m'
BRED = '\033[41m'
BMAG = '\033[45m'
ENDC = '\033[0m'
def p(something, color):
    print(color + something + ENDC)

pic="./img/1.png"
url_media = "https://api.twitter.com/1.1/statuses/update_with_media.json"

followers = api.followers_ids(SN)
friends = api.friends_ids(SN)

def find_all_files(directory):
    for root, dirs, files in os.walk(directory):
        yield root
        for file in files:
            yield os.path.join(root, file)

def words(api = tweepy.API(auth)):

    words = ['sougofollow',
             'followme',
             'followdaibosyu',
             'sougofollow',
             'フォローミーjp', 
             '相互フォロー',
             'フォロー大募集',
             '相互フォロー',
             'フォロワー増やしたい',
             'followmeJP',
             'sougo',
             'Autofollow',
             'follow',
             'リツイートした人全員フォロー',
             '相互フォロー募集中',
             'フォロー返し']
    word = str(random.choice(words))
    print(word)
    count = 1000
    return api.search(q=word, count=count)


def speak():
    timestamp = 'date +%F_%H:%M:%S'
    current_time = os.popen(timestamp).readline().strip()
    return api.update_status(
        status="現在の時刻は " +
        current_time+
        "です。"+
        "相互フォロー歓迎いたします。"+
        "#フォローミーjp"+
        "#フォロー大募集"+
        "#相互フォロー"+
        "#フォロワー増やしたい")

def speak_with_image(file_list):
    
    img_name = str(random.choice(file_list))
    files = {
        "status": "Who is this? "+
        " #フォロー"+
        " #フォロー"+
        " #フォロー返し"+
        " #フォローミー"+
        " #フォロー募集" +
        " #フォロー歓迎"+
        " #フォローしてね"+
        " #フォロワー募集"+
        " #フォローバック"+
        " #リツイートした人全員フォローする"+
        " #RTした人全員フォローする"+
        " #follow"+
        " #followme"+
        " #followmeJP"+
        " #followback"+
        " #相互フォロー ",
        "media[]" : open(img_name, 'rb')
    }
    twitter = OAuth1Session(CK, CS, AT, AS)
    return twitter.post(url_media, files = files)

def speak_with_rss():
    
    #RSS URL
    rss_lists = ["https://www.lifehacker.jp/feed/index.xml",
                 "http://rss.rssad.jp/rss/nikkansports/baseball/atom.xml",
                 "https://www.nikkansports.com/soccer/atom.xml",
                 "http://rss.rssad.jp/rss/nikkansports/sports/atom.xml",
                 "http://rss.rssad.jp/rss/nikkansports/golf/atom.xml",
                 "http://rss.rssad.jp/rss/nikkansports/battle/atom.xml",
                 "http://p.nikkansports.com/goku-uma/rss/atom.xml",
                 "http://rss.rssad.jp/rss/nikkansports/entertainment/atom.xml"]

    rss_list  = str(random.choice(rss_lists))

    #SSL
    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context
        
        #RSS取得
        feed = feedparser.parse(rss_list)
        
        #RSS title
        print(feed.feed.title)


        entry_link_list = []
        entry_summary_list = []
        #RSS  published link
        for entry in feed.entries:
            # print(len(feed.entries))
            # print(entry.published)
            entry_summary_list.append(entry.summary)
            entry_link_list.append(entry.link)
        link = str(random.choice(entry_link_list))
        summary = str(random.choice(entry_summary_list)) 
        print(link, summary)

    return api.update_status(status="#follow "+ link + " "+ summary)

def GetName(result):
    return  result.user.name

def GetUserId(result):
    return  result.user.id 

def GetUserInfo(result):
    return  result.user._json['screen_name']

def FollowAndlike(search_results, file_list):
    i= 0
    for result in search_results:
        i += 1
        username = GetUserInfo(result)
        user_id = GetUserId(result)  # Get user_id
        print("user_id",i ,": ",user_id)
        user = GetName(result)  # Get name
        print('user',': ', user)
        tweet = result.text
        print('tweet',': ', tweet)
        t = result.created_at
        try:
            speak_with_rss()
            print("Tweet Done!")
        except:
            print("Tweet error")
        try:
            api.create_favorite(user_id) 
            print(user)
            p("Like Done", BMAG)
        except:
            p("Couldn't like it", BRED)
            p("-"*40, BLUE)
        try:
            api.create_friendship(user_id)
            p("Follow Done", BLUE)
            p("*"*40, BLUE)
        except:
            p("Already followed him", BRED)
            p("-"*40, BLUE)
        try:
            api.retweet(user_id)
            p("Retweet Done", BLUE)
            p("-"*40, BLUE)
        except:
            p("Retweet Error", BRED)
        try:
            unfollow(api, followers, friends)
            p("UnFollow Done", BLUE)
            p("*"*40, BLUE)
        except:
            p("Unfollow Error", BRED)
        time.sleep(int(interval))

def unfollow(api,followers, friends):
    f = str(random.choice(friends))    
    try:
        if f not in followers:
            api.destroy_friendship(f)
            p("{0} Bye!".format(api.get_user(f).screen_name), BLUE)
    except Exception as e:
        print("Unfoolow Error")

def main():
    search_results = words()    
    for file in find_all_files('./img/image-data/face/'):
        file_list.append(file)
    FollowAndlike(search_results, file_list)

if __name__ == "__main__":
    for k in range(1000000):
        main()
        print("Loop", k, " Done")
        # loop interval
        time.sleep(3)

